#include <stdio.h>

void swap(int x, int y) {
  int tmp;
  tmp = x;
  x = y;
  y = tmp;
  printf("Dentro: x:%i y:%i\n", x, y);
}

void swapR(int *x, int *y) {
  int tmp;
  tmp = *x;
  *x = *y;
  *y = tmp;
  printf("Dentro Referencia: x:%i y:%i\n", *x, *y);
}

int main(int argc, char **argv) {
  int x = 5;
  int y = 8;

  printf("x:%i y:%i direccion x:%p\n", x, y, &x);

  swap(x, y);
  printf("Valor: x:%i y:%i\n", x, y);
  swapR(&x, &y);
  printf("Referencia: x:%i y:%i\n", x, y);
  return 0;
}