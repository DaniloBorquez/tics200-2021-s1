public class Alumno extends Persona {

    private double nota;

    public Alumno(String nombre, String apellido, double nota) {
        super(nombre, apellido);
        this.nota = nota;
    }

    public double getNota() {
        return this.nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public String toString() {
        return "Alumno:" + this.nombre + " " + this.apellido + "\nNota: " + this.nota;
    }

}