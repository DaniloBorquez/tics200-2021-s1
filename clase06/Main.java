import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Persona p1 = new Persona(1, "Danilo", "Borquez");
        Persona p2 = new Persona(2, "Daniel", "Borquez");
        Persona p3 = new Persona("Sandra", "Borquez");
        Persona p4 = new Persona("Sandra", "Valenzuela");

        Profesor pp1 = new Profesor("Simon", "Gomez", "Matematicas");
        Alumno aa1 = new Alumno("Javier", "Merino", 7.5);

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);

        System.out.println(pp1);
        System.out.println(aa1);

        ArrayList<Persona> personas = new ArrayList<>();
        personas.add(p1);
        personas.add(p2);
        personas.add(p3);
        personas.add(p4);
        personas.add(pp1);
        personas.add(aa1);
        System.out.println("Mostrando desde arraylist");
        for (Persona persona : personas) {
            System.out.println(persona);
        }

    }
}