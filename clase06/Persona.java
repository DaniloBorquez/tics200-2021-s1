public class Persona {
    protected int id;
    protected String nombre;
    protected String apellido;
    static private int contador = 0;

    public Persona() {
    }

    public Persona(int id, String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = id;
        contador++;
    }

    public Persona(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = ++contador;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String toString() {
        return id + ": " + this.nombre + " " + this.apellido;
    }
}