from Persona import Persona

if __name__ == '__main__':
    p1 = Persona("Danilo", "Borquez")
    p2 = Persona("Pedro", "Borquez")
    print(p1.to_string())
    print(p2.to_string())
    print(Persona.metodo())
