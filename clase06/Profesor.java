public class Profesor extends Persona {

    private String area;

    public Profesor(String nombre, String apellido, String area) {
        super(nombre, apellido);
        this.area = area;
    }

    public String getArea() {
        return this.area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String toString() {
        return "Profesor:" + this.nombre + " " + this.apellido + "\nArea: " + this.area;
    }

}