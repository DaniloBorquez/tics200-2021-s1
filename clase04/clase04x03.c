#include <stdio.h>

void swap_valor(int x, int y) {
  int tmp = x;
  x = y;
  y = tmp;
  printf("Dentro valor--> a: %i | b: %i\n", x, y);
}

void swap_ref(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
  printf("Dentro referencia--> a: %i | b: %i\n", *x, *y);
}

int main(int argc, char *argv[]) {
  int a = 10;
  int b = 15;
  printf("a: %i | b: %i\n", a, b);
  swap_valor(a, b);
  printf("Despues Valor-->a: %i | b: %i\n", a, b);
  swap_ref(&a, &b);
  printf("Despues referencia-->a: %i | b: %i\n", a, b);
  return 0;
}