#include <stdio.h>

int main(int argc, char* argv[]) {
  int x = 1;

  while (x != 0) {
    scanf("%i", &x);
    switch (x) {
      case 1:
        printf("Es un uno!\n");
      case 2:
        printf("Es un dos!\n");
        break;
      case '3':
        printf("Es un tres!\n");
        break;
      default:
        printf("No es ni un uno, ni un dos, ni un tres!\n");
        break;
    }
  }
  return 0;
}