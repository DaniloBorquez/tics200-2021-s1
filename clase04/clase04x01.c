#include <stdio.h>

int main(int argc, char* argv[]) {
  char frase[5] = "hola";
  char* f = "hola";

  printf("%s\n", frase);
  printf("%s\n", f);
  printf("%c\n", frase[2]);
  printf("%c\n", f[2]);

  // frase[2] = 'd';
  f = "chaooo";

  printf("%s\n", frase);
  printf("%s\n", f);
  return 0;
}