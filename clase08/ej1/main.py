from persona import Persona

if __name__ == '__main__':
    p = Persona()
    p.displayEdad()
    print(p._Persona__edad)
    p._Persona__edad = 15
    p.displayEdad()

    print("edad desde property", p.edad)
    p.displayEdad()

    p.edad = 7
    print("edad desde property", p.edad)
    p.displayEdad()

    try:
        p.edad = -7
    except:
        nuevaEdad = int(input("Ingresa edad correcta: "))
        p.edad = nuevaEdad
    print("edad desde property", p.edad)
    p.displayEdad()
