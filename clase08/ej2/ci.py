class InvalidDV(Exception):
    def __init__(self, message):
        self.message = message


class DVNotMatching(Exception):
    def __init__(self, message):
        self.message = message


class RUTNotMatching(Exception):
    def __init__(self, message):
        self.message = message


class CI():
    def validar(rut, dv):
        mult = [2, 3, 4, 5, 6, 7]
        num_str = str(rut)[::-1]
        sum = 0
        cont = 0
        if isinstance(dv, str):
            dv = dv.lower()
        for i in num_str:
            sum += int(i)*mult[cont % 6]
            cont += 1
        tmpDv = (11 - sum % 11) if (11 - sum % 11) != 10 else "k"
        if tmpDv == dv:
            return True
        else:
            return False

    def __init__(self, num, dv):
        if not isinstance(num, int):
            raise RUTNotMatching("El rut ingresado no es número")
        else:
            self.__num = num
        if not isinstance(dv, int) and not isinstance(dv, str):
            raise DVNotMatching(
                "El digito verificador ingresado no es número ni string")
        else:
            self.__num = num

        if CI.validar(num, dv):
            self.__dv = dv
        else:
            raise InvalidDV("Dígito verificador inválido")

    def toString(self):
        return str(self.__num)+"-"+str(self.__dv)
