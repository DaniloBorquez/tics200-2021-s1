from ci import *

if __name__ == '__main__':
    try:
        cedula = CI(16452837, 5)
        print(cedula.toString())
    except InvalidDV as e1:
        print("Digito verificador no corresponde con el rut")
        print("El error dice:", e1)
    except DVNotMatching as e2:
        print("Digito verificador no es ni número ni string")
        print("El error dice:", e2)
    except RUTNotMatching as e3:
        print("Rut no es un número!")
        print("El error dice:", e3)
