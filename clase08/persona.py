class Persona():
    def __init__(self):
        self.__edad = 0

    def displayEdad(self):
        print("Mi edad es", self.__edad)

    @property
    def edad(self):
        if self.__edad > 10:
            return 10
        else:
            return self.__edad

    @edad.setter
    def edad(self, edad):
        if edad < 0:
            raise Exception("Se introdujo una edad menor a 0")
        else:
            self.__edad = edad
