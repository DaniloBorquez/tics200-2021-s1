#include <math.h>
#include <stdio.h>

int main(int argc, char** argv) {
  int a = 3;
  int b = 4;
  int z = 7;
  int c = a == 3;
  int d = a == b;
  int e = a == b && a == 3;
  int f = a == b || a == 3;
  int g = a & z;   // a: 11 z: 111 ---> a&z   011 --> 3
  int h = a << 2;  // a: 11 --->> a : 1100 ---> 12
  printf("%i\n", c);
  printf("%i\n", d);
  printf("%i\n", e);
  printf("%i\n", f);
  printf("%i\n", g);
  printf("%i\n", h);
  return 0;
}
