#include <stdio.h>

int main(int argc, char** argv) {
  int x = 5;
  float y = 5.0;
  int* p;
  float* pf;
  int arreglo[10];

  for (int i = 0; i < 10; i++) {
    arreglo[i] = i * 3;
  }

  p = &x;
  pf = &y;
  printf("X Valor %i\n", x);
  printf("X dirección %p\n", &x);

  printf("P Valor %p\n", p);
  printf("P dirección %p\n", &p);
  printf("P apuntado %i\n\n", *p);

  x = 10;
  printf("Xn Valor %i\n", x);
  printf("Xn dirección %p\n", &x);

  printf("Pn Valor %p\n", p);
  printf("Pn dirección %p\n", &p);
  printf("Pn apuntado %i\n\n", *p);

  for (int i = 0; i < 10; i++) {
    printf("%i ", arreglo[i]);
  }
  printf("\n");
  printf("%i\n", *arreglo);
  printf("%p\n", arreglo);
  printf("%i\n", *(arreglo + 3));
}
