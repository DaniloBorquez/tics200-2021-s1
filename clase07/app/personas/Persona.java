package app.personas;

public class Persona {
    private String nombre;
    private String apellido;

    public Persona() {

    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String toString() {
        return this.nombre + " " + this.apellido;
    }
}