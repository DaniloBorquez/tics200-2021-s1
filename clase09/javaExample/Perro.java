public class Perro extends Animal implements Sonoro {
    public Perro() {
        super("Perro");
    }

    public String aboutMe() {
        return "Hola! soy un perrito =)";
    }

    public String hacerSonido() {
        return "guau";
    }
}