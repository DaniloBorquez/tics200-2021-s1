import java.util.ArrayList;

/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        // Animal animal = new Animal("Perro");
        Perro p = new Perro();
        Gato g = new Gato();

        System.out.println(p.aboutMe());
        System.out.println(g.aboutMe());
        System.out.println(p.getTipo());

        ArrayList<Sonoro> animales = new ArrayList<>();
        animales.add(p);
        animales.add(g);

        for (Sonoro a : animales) {
            // System.out.println(animal.aboutMe());
            Animal animal = (Animal) a;
            if (animal.getTipo().equals("Perro")) {
                Perro perro = (Perro) animal;
                System.out.println(perro.hacerSonido());
            } else if (animal.getTipo().equals("Gato")) {
                Gato gato = (Gato) animal;
                System.out.println(gato.hacerSonido());
            }
        }
    }
}