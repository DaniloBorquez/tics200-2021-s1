public class Gato extends Animal implements Sonoro {
    public Gato() {
        super("Gato");
    }

    public String aboutMe() {
        return "Hola! soy un gatito =)";
    }

    public String hacerSonido() {
        return "prrrrrrrrrrrr";
    }
}