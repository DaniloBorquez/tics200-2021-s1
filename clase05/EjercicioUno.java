public class EjercicioUno {
    private String nombre;

    public EjercicioUno(String nombre) {
        this.nombre = nombre;
        System.out.println("Construyendo un objeto " + nombre);
    }

    public String mensaje() {
        return "El mensaje que yo quiera desde " + this.nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}