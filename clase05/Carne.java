public class Carne {
    private String nombres;
    private String apellidos;
    private String rut;
    private int numeroDocumento;

    static private int documentosCreados = 0;

    public Carne(String nombres, String apellidos, String rut) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.rut = rut;
        this.numeroDocumento = ++documentosCreados;
    }

    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return this.apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getRut() {
        return this.rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getNumeroDocumento() {
        return this.numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String toString() {
        return "ID: " + this.numeroDocumento + "\nNombres: " + this.nombres + "\nApellidos: " + this.apellidos;
    }
}